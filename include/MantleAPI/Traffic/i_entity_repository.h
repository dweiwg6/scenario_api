/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_entity_repository.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_IENTITYREPOSITORY_H
#define MANTLEAPI_TRAFFIC_IENTITYREPOSITORY_H

#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_entity.h>

#include <functional>
#include <optional>
#include <string>
#include <vector>

namespace mantle_api
{
/// This interface provides CRUD functionality for scenario entities.
class IEntityRepository
{
public:
  virtual IVehicle& Create(const std::string& name, const VehicleProperties& properties) = 0;
  [[deprecated]] virtual IVehicle& Create(UniqueId id, const std::string& name, const VehicleProperties& properties) = 0;
  virtual IPedestrian& Create(const std::string& name, const PedestrianProperties& properties) = 0;
  [[deprecated]] virtual IPedestrian& Create(UniqueId id, const std::string& name, const PedestrianProperties& properties) = 0;
  virtual IStaticObject& Create(const std::string& name, const mantle_api::StaticObjectProperties& properties) = 0;
  [[deprecated]] virtual IStaticObject& Create(UniqueId id, const std::string& name, const StaticObjectProperties& properties) = 0;

  virtual IVehicle& GetHost() = 0;
  virtual std::optional<std::reference_wrapper<IEntity>> Get(const std::string& name) = 0;
  virtual std::optional<std::reference_wrapper<const IEntity>> Get(const std::string& name) const = 0;
  virtual std::optional<std::reference_wrapper<IEntity>> Get(UniqueId id) = 0;
  virtual std::optional<std::reference_wrapper<const IEntity>> Get(UniqueId id) const = 0;
  virtual bool Contains(UniqueId id) const = 0;

  virtual void Delete(const std::string& name) = 0;
  virtual void Delete(UniqueId id) = 0;

  virtual const std::vector<std::unique_ptr<mantle_api::IEntity>>& GetEntities() const = 0;

  virtual void RegisterEntityCreatedCallback(const std::function<void(IEntity&)>& callback) = 0;
  virtual void RegisterEntityDeletedCallback(const std::function<void(const std::string&)>& callback) = 0;
  virtual void RegisterEntityDeletedCallback(const std::function<void(UniqueId)>& callback) = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_IENTITYREPOSITORY_H
