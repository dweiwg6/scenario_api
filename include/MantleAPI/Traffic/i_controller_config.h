/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_controller_config.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_ICONTROLLERCONFIG_H
#define MANTLEAPI_TRAFFIC_ICONTROLLERCONFIG_H

#include <MantleAPI/Common/route_definition.h>
#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Map/i_lane_location_query_service.h>
#include <MantleAPI/Traffic/control_strategy.h>

#include <algorithm>
#include <map>
#include <memory>
#include <vector>

namespace mantle_api
{
struct IControllerConfig
{
  IControllerConfig() {}
  IControllerConfig(const IControllerConfig& controller_config)
      : map_query_service(controller_config.map_query_service)
  {
    std::transform(controller_config.control_strategies.begin(),
                   controller_config.control_strategies.end(),
                   std::back_inserter(control_strategies),
                   [](auto& control_strategy) { return std::make_unique<mantle_api::ControlStrategy>(*control_strategy); });
  }

  virtual ~IControllerConfig() = default;

  // TODO: Check why map_query_service is part of the interface because it is not set from engine side but only in the
  // environment on calling AddController()
  ILaneLocationQueryService* map_query_service{nullptr};
  std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
  RouteDefinition route_definition;
};

inline bool operator==(const IControllerConfig& lhs, const IControllerConfig& rhs) noexcept
{
  bool control_strategies_equal = true;
  if (lhs.control_strategies.size() != rhs.control_strategies.size())
  {
    control_strategies_equal = false;
  }
  else
  {
    for (unsigned int i = 0; i < lhs.control_strategies.size(); i++)
    {
      if (*(lhs.control_strategies[i]) != *(rhs.control_strategies[i]))
      {
        control_strategies_equal = false;
        break;
      }
    }
  }
  return control_strategies_equal;
}

struct NoOpControllerConfig : public IControllerConfig
{
};

struct InternalControllerConfig : public IControllerConfig
{
};

struct ExternalControllerConfig : public IControllerConfig
{
  std::string name;
  std::map<std::string, std::string> parameters;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_ICONTROLLERCONFIG_H
